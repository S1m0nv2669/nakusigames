﻿using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class BombSpawner
{
    private const int MAX_BOMB_COUNT = 10;
    private const int THREAD_SLEEP_TIME = 10;

    private List<BaseBomb> _bombsList;

    private BombSpawnerPresenter _bombSpawnerPresenter;
    private CharactersSpawner _charactersSpawner;

    private Thread _bombMovingThread;
    private EventWaitHandle ChildWaitHandler = new EventWaitHandle(true, EventResetMode.ManualReset);

    private bool _isStop;

    public BombSpawner(BombSpawnerView bombSpawnerView, CharactersSpawner charactersSpawner)
    {
        _bombSpawnerPresenter = new BombSpawnerPresenter(bombSpawnerView);
        _charactersSpawner = charactersSpawner;
    }

    public void SpawnBomb(Vector2 mapSize, float height)
    {
        _bombsList = new List<BaseBomb>();

        for (int i = 0; i < MAX_BOMB_COUNT; i++)
        {
            Vector3 position = new Vector3(Random.Range(0, mapSize.x), height, Random.Range(0, mapSize.y));
            _bombsList.Add(new BaseBomb(position, _bombSpawnerPresenter, _charactersSpawner, this));
        }

        StartMoving();
    }

    public void RemoveBomb(BaseBomb bomb)
    {
        bomb.DestroyBombVisual();
        _bombsList.Remove(bomb);
    }

    private void StartMoving()
    {
        _bombMovingThread = new Thread(Moving);
        _bombMovingThread.Start();

        ChildWaitHandler.Set();
    }

    private void Moving()
    {
        //ChildWaitHandler.Reset();
        //ChildWaitHandler.WaitOne();

        while (true)
        {
            Debug.Log("Moving");

            if (_isStop)
            {
                Debug.Log("Stop thread");
                ChildWaitHandler.Reset();
                ChildWaitHandler.WaitOne();
            }

            for (int index = 0; index < _bombsList.Count; index++)
            {
                BaseBomb curBomb = _bombsList[index];

                if (curBomb == null)
                {
                    _bombsList.RemoveAt(index);
                    index--;
                }

                _bombsList[index].Moving();
            }

            Thread.Sleep(THREAD_SLEEP_TIME);
        }  
    }

    private void OnApplicationQuit()
    {
        _isStop = true;
    }
}
