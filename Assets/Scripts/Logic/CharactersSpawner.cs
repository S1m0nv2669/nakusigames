﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersSpawner
{
    private const int MAX_CHARACTERS_COUNT = 10;

    private List<Character> _charactersList;

    private CharactersSpawnerPresenter _charactersSpawnerPresenter;

    public CharactersSpawner(CharactersSpawnerView charactersSpawnerView)
    {
        _charactersSpawnerPresenter = new CharactersSpawnerPresenter(charactersSpawnerView);
    }

    public void SpawnCharacters(Vector2 mapSize, float height)
    {
        _charactersList = new List<Character>();

        for (int i = 0; i < MAX_CHARACTERS_COUNT; i++)
        {
            Vector3 position = new Vector3(Random.Range(0, mapSize.x), height, Random.Range(0, mapSize.y));
            _charactersList.Add(new Character(position, _charactersSpawnerPresenter, this));
        }   
    }

    public void RemoveCharacter(Character character)
    {
        character.DestroyCharacterVisual();
        _charactersList.Remove(character);
    }

    public List<Character> GetCharactersList()
    {
        return _charactersList;
    }

}
