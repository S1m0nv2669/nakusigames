﻿using UnityEngine;

public class SimpleBomb : BaseBomb
{
    public SimpleBomb(Vector3 position, BombSpawnerPresenter bombSpawnerPresenter, CharactersSpawner charactersSpawner, BombSpawner bombSpawner) : base(position, bombSpawnerPresenter, charactersSpawner, bombSpawner)
    {
    }
}
