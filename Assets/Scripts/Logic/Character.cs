﻿using UnityEngine;

public class Character : ITarget
{
    private const int MAX_HP = 10; //todo move to config

    private Vector3 _position;
    private int _currentHP;
    private readonly CharactersSpawnerPresenter _charactersSpawnerPresenter;
    private readonly CharactersSpawner _charactersSpawner;
    private CharacterController _characterController;

    public Character(Vector3 position, CharactersSpawnerPresenter charactersSpawnerPresenter, CharactersSpawner charactersSpawner)
    {
        _charactersSpawnerPresenter = charactersSpawnerPresenter;
        _charactersSpawner = charactersSpawner;

        _position = position;
        _currentHP = MAX_HP;

        _characterController = _charactersSpawnerPresenter.CreateCharacterVisual(position);
    }

    public Vector3 GetPosition()
    {
        return _position;
    }

    public void DestroyCharacterVisual()
    {
        if (_characterController != null)
        {
            _characterController.Destroy();
        }
        else
        {
            Debug.Log("No _characterController");
        }
    }

    void ITarget.DoDamage(int damage)
    {
        _currentHP -= damage;

        if (_currentHP <= 0)
        {
            DestroyCharacter();
        }
    }

    private void DestroyCharacter()
    {
        _charactersSpawner.RemoveCharacter(this);
    }
}
