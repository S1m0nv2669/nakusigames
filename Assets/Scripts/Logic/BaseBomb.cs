﻿using System.Collections.Generic;
using UnityEngine;

public class BaseBomb
{
    private const int DAMAGE = 5; //todo move to config
    private const float SPEED = 0.05f;
    private const float EXPLODE_HEIGHT = 0f;
    private const float EXPLODE_RANGE = 3f;

    private Vector3 _position;
    private readonly BombSpawnerPresenter _bombSpawnerPresenter;
    private readonly CharactersSpawner _charactersSpawner;
    private readonly BombSpawner _bombSpawner;

    private BombController _bombController;

    public BaseBomb(Vector3 position, BombSpawnerPresenter bombSpawnerPresenter, CharactersSpawner charactersSpawner, BombSpawner bombSpawner)
    {
        _bombSpawnerPresenter = bombSpawnerPresenter;
        _charactersSpawner = charactersSpawner;
        _bombSpawner = bombSpawner;

        _position = position;

        _bombController = _bombSpawnerPresenter.CreateBombVisual(position);
    }

    public void Moving()
    {
        Debug.Log("Moving");
        _position -= Vector3.up * SPEED;
        _bombController.UpdatePosition(_position);

        if (_position.y <= EXPLODE_HEIGHT)
        {
            Explode();
        }
    }

    public void DestroyBombVisual()
    {
        if (_bombController != null)
        {
            _bombController.Destroy();
        }
        else
        {
            Debug.Log("No _bombController");
        }
    }

    private void Explode()
    {
        List<Character> charList = _charactersSpawner.GetCharactersList();

        for (int index = 0; index < charList.Count; index++)
        {
            if (charList[index] == null)
            {
                charList.RemoveAt(index);
                index--;
            }

            if (DamageCondition(charList[index].GetPosition()))
            {
                (charList[index] as ITarget).DoDamage(DAMAGE);
            }
        }

        DestroySelf();
    }

    protected virtual bool DamageCondition(Vector3 charPosition)
    {
        if (Vector3.Distance(_position, charPosition) <= EXPLODE_RANGE)
        {
            return true;
        }

        return false;
    }

    private void DestroySelf()
    {
        _bombSpawner.RemoveBomb(this);
    }
}
