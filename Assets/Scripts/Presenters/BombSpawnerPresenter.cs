﻿using UnityEngine;

public class BombSpawnerPresenter 
{
    private readonly BombSpawnerView _bombSpawnerView;

    public BombSpawnerPresenter(BombSpawnerView bombSpawnerView)
    {
        _bombSpawnerView = bombSpawnerView;
    }

    public BombController CreateBombVisual(Vector3 position)
    {
        if (_bombSpawnerView != null)
        {
           return _bombSpawnerView.CreateBombView(position);
        }
        else
        {
           Debug.Log("No Bomb's visualisation");
        }

        return null;
    }
}
