﻿using UnityEngine;

public class CharactersSpawnerPresenter
{
    private readonly CharactersSpawnerView _charactersSpawnerView;

    public CharactersSpawnerPresenter(CharactersSpawnerView charactersSpawnerView)
    {
        _charactersSpawnerView = charactersSpawnerView;
    }

    public CharacterController CreateCharacterVisual(Vector3 position)
    {
        if (_charactersSpawnerView != null)
        {
           return _charactersSpawnerView.CreateCharacterView(position);
        }
        else
        {
            Debug.Log("No Character's visualisation");
        }

        return null;
    }
}
