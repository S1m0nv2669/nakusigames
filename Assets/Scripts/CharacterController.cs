﻿using UnityEngine;

public class CharacterController : MonoBehaviour
{
    private bool _isNeedDestroy;

    private void Update()
    {
        if (_isNeedDestroy)
        {
            Destroy(this.gameObject);
        }
    }

    public void Destroy()
    {
        _isNeedDestroy = true;
    }   
}
