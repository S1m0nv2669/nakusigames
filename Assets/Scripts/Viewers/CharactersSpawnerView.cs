﻿using UnityEngine;

//общее вью со ссылками на все визуальные объекты игроков
public class CharactersSpawnerView : MonoBehaviour
{
    [SerializeField]
    private GameObject _charactersViewPrefab;
    [SerializeField]
    private Transform _mapTransform;

    public CharacterController CreateCharacterView(Vector3 position)
    {
        GameObject tmpGO = Instantiate(_charactersViewPrefab, _mapTransform);
        tmpGO.transform.position = position;

        return tmpGO.GetComponent<CharacterController>();
    }
}
