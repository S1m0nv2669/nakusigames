﻿using UnityEngine;

public class BombSpawnerView : MonoBehaviour
{
    [SerializeField]
    private GameObject _bombViewPrefab;
    [SerializeField]
    private Transform _mapTransform;

    public BombController CreateBombView(Vector3 position)
    {
        GameObject tmpGO = Instantiate(_bombViewPrefab, _mapTransform);
        tmpGO.transform.position = position;

        return tmpGO.GetComponent<BombController>();
    }
}
