﻿using UnityEngine;

public class AppStarter : MonoBehaviour
{
    [SerializeField]
    private Vector2 _mapSize;
    [SerializeField]
    private CharactersSpawnerView _charactersSpawnerView;
    [SerializeField]
    private BombSpawnerView _bombSpawnerView;

    private CharactersSpawner _charactersSpawner;
    private BombSpawner _bombSpawner;

    void Start()
    {
        _charactersSpawner = new CharactersSpawner(_charactersSpawnerView);
        _charactersSpawner.SpawnCharacters(_mapSize, height: 0f);

        _bombSpawner = new BombSpawner(_bombSpawnerView, _charactersSpawner);
        _bombSpawner.SpawnBomb(_mapSize, height : 5f);
    }
}
