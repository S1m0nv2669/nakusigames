﻿using UnityEngine;

public class BombController : MonoBehaviour
{
    private Vector3 _currentPosition;
    private bool _isNeedDestroy;

    private void Update()
    {
        transform.position = _currentPosition;

        if (_isNeedDestroy)
        {
            Destroy(this.gameObject);
        }
    }

    public void UpdatePosition(Vector3 position)
    {
        _currentPosition = position;
    }

    public void Destroy()
    {
        _isNeedDestroy = true;
    }
}
